package es.edataconsulting.backend.apirest.entity;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class PersonTest {

	private Person person;

	@Before
	public void setUp() throws Exception {
		person = new Person();
	}

	@Test
	public void testSetAndGetId() {
		
		Long testId = Long.valueOf(1);
		assertNull(person.getId());
		person.setId(testId);
		assertEquals(testId, person.getId());
		
	}

	@Test
	public void testSetAndGetName() {
		
		String testName = "Name";
		assertNull(person.getName());
		person.setName(testName);
		assertEquals(testName, person.getName());
		
	}

	@Test
	public void testSetAndGetUsername() {
		
		String testUsername = "Username";
		assertNull(person.getUsername());
		person.setUsername(testUsername);
		assertEquals(testUsername, person.getUsername());
		
	}

	@Test
	public void testSetAndGetPassword() {
		
		String testPassword = "Password";
		assertNull(person.getPassword());
		person.setPassword(testPassword);
		assertEquals(testPassword, person.getPassword());
		
	}

	@Test
	public void testSetAndGetRoles() {
		
		List<Role> testRoles = new ArrayList<Role>();
		Role testRole1 = new Role();
		Role testRole2 = new Role();

		testRole1.setId(Long.valueOf(1));
		testRole1.setName("Admin");

		testRole2.setId(Long.valueOf(2));
		testRole2.setName("Standard");

		testRoles.add(testRole1);
		testRoles.add(testRole2);

		assertNull(person.getRoles());
		person.setRoles(testRoles);
		assertEquals(testRoles, person.getRoles());
		
	}

}
