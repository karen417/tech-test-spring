package es.edataconsulting.backend.apirest.entity;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class RoleTest {
	
	private Role role;

	@Before
	public void setUp() throws Exception {
		role = new Role();
	}

	@Test
	public void testSetAndGetId() {
		
		Long testId = Long.valueOf(1);
		assertNull(role.getId());
		role.setId(testId);
		assertEquals(testId, role.getId());
		
	}

	@Test
	public void testSetAndGetName() {
		
		String testName = "Name";
		assertNull(role.getName());
		role.setName(testName);
		assertEquals(testName, role.getName());
		
	}

}
