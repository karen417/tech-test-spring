package es.edataconsulting.backend.apirest.service;

import java.util.List;

import es.edataconsulting.backend.apirest.entity.Role;

public interface IRoleService {

	public List<Role> findAll();

	public Role findById(Long id);

	public Role findByName(String name);

	public Role save(Role role);

	public void delete(Long id);
}
