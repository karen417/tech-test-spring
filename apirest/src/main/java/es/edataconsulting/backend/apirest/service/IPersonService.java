package es.edataconsulting.backend.apirest.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import es.edataconsulting.backend.apirest.entity.Person;

public interface IPersonService {

	public List<Person> findAll();

	public Page<Person> findAll(Pageable pageable);

	public Person findById(Long id);

	public Person findByUsername(String username);

	public Person save(Person person);

	public void delete(Long id);

}
