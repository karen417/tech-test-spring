package es.edataconsulting.backend.apirest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.edataconsulting.backend.apirest.entity.Role;
import es.edataconsulting.backend.apirest.repository.RoleRepository;

@Service
public class RoleServiceImpl implements IRoleService {

	@Autowired
	private RoleRepository roleRepo;

	@Override
	public List<Role> findAll() {
		return roleRepo.findAll();
	}

	@Override
	public Role findById(Long id) {
		return roleRepo.findById(id).orElse(null);
	}

	@Override
	public Role save(Role role) {
		return roleRepo.save(role);
	}

	@Override
	public void delete(Long id) {
		roleRepo.deleteById(id);
	}

	@Override
	public Role findByName(String name) {
		return roleRepo.findByName(name);
	}

}
