package es.edataconsulting.backend.apirest.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.edataconsulting.backend.apirest.entity.Role;
import es.edataconsulting.backend.apirest.entity.Person;
import es.edataconsulting.backend.apirest.service.IRoleService;
import es.edataconsulting.backend.apirest.service.IPersonService;

@CrossOrigin(origins= {"http://localhost:4200"})
@RestController
@RequestMapping("/api/users")
public class PersonRestController {

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@Autowired
	private IPersonService personService;

	@Autowired
	private IRoleService roleService;

	@GetMapping("/")
	public List<Person> index() {
		return personService.findAll();
	}

	@GetMapping("/page/{page}")
	public Page<Person> index(@PathVariable Integer page) {
		return personService.findAll(PageRequest.of(page, 5));
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> show(@PathVariable Long id) {

		Person person = null;

		Map<String, Object> response = new HashMap<>();

		try {
			person = personService.findById(id);
		} catch (DataAccessException e) {
			response.put("message", "Data access error.");
			response.put("error", e.getMessage() + ": " + e.getMostSpecificCause().getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (person == null) {
			response.put("message", "User id " + id.toString() + " does not exist.");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Person>(person, HttpStatus.OK);
		
	}

	@Secured({ "ROLE_ADMIN" })
	@PostMapping("/")
	public ResponseEntity<?> create(@Valid @RequestBody Person person, BindingResult result) {

		Person newPerson = null;
		Map<String, Object> response = new HashMap<>();

		if (result.hasErrors()) {
			List<String> errors = result.getFieldErrors().stream()
					.map(err -> "Field '" + err.getField() + "' " + err.getDefaultMessage())
					.collect(Collectors.toList());
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		// Set user a default role if not specified
		if (person.getRoles() == null || person.getRoles().isEmpty()) {
			String role = "ROLE_STANDARD";
			Role defaultRole = roleService.findByName(role);

			if (defaultRole == null) {
				response.put("message", "Error: default role '" + role
						+ "' does not exist. Please create default role first or select an existing role.");
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
			} else {
				List<Role> defaultRoleList = new ArrayList<>();
				defaultRoleList.add(defaultRole);
				person.setRoles(defaultRoleList);
			}
		}

		try {
			String passwordBcrypt = passwordEncoder.encode(person.getPassword());
			person.setPassword(passwordBcrypt);
			newPerson = personService.save(person);
		} catch (DataAccessException e) {
			response.put("message", "Data access error.");
			response.put("error", e.getMessage() + ": " + e.getMostSpecificCause().getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("message", "The user has been successfully created!");
		response.put("person", newPerson);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

	}

	@Secured({ "ROLE_ADMIN" })
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody Person person, BindingResult result, @PathVariable Long id) {

		Person updatedPerson = null;
		Person actualPerson = null;
		Map<String, Object> response = new HashMap<>();

		if (result.hasErrors()) {
			List<String> errors = result.getFieldErrors().stream()
					.map(err -> "Field '" + err.getField() + "' " + err.getDefaultMessage())
					.collect(Collectors.toList());
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		try {
			actualPerson = personService.findById(id);
		} catch (DataAccessException e) {
			response.put("message", "Data access error.");
			response.put("error", e.getMessage() + ": " + e.getMostSpecificCause().getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (actualPerson == null) {
			response.put("message", "User id" + id.toString() + " does not exist.");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		if (!actualPerson.getPassword().equals(person.getPassword())) {
			String passwordBcrypt = passwordEncoder.encode(person.getPassword());
			actualPerson.setPassword(passwordBcrypt);
		} else {
			actualPerson.setPassword(person.getPassword());
		}
		
		actualPerson.setName(person.getName());
		
		if (person.getRoles() != null && !person.getRoles().isEmpty()) {
			actualPerson.setRoles(person.getRoles());
		}

		try {
			updatedPerson = personService.save(actualPerson);
		} catch (DataAccessException e) {
			response.put("message", "Data access error.");
			response.put("error", e.getMessage() + ": " + e.getMostSpecificCause().getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("message", "The user has been successfully updated!");
		response.put("person", updatedPerson);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
		
	}

	@Secured({ "ROLE_ADMIN" })
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {

		Map<String, Object> response = new HashMap<>();

		try {
			personService.delete(id);
		} catch (DataAccessException e) {
			response.put("message", "Data access error.");
			response.put("error", e.getMessage() + ": " + e.getMostSpecificCause().getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("message", "The user has been successfully deleted!");

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

}
