package es.edataconsulting.backend.apirest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import es.edataconsulting.backend.apirest.entity.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

	public Role findByName(String name);
}
