package es.edataconsulting.backend.apirest.service;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.edataconsulting.backend.apirest.entity.Person;
import es.edataconsulting.backend.apirest.repository.PersonRepository;

@Service
public class PersonServiceImpl implements IPersonService, UserDetailsService {

	private Logger logger = LoggerFactory.getLogger(PersonServiceImpl.class);

	@Autowired
	private PersonRepository personRepo;

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		Person person = personRepo.findByUsername(username);

		if (person == null) {
			logger.error("Error: user '" + username + "' does not exist.");
			throw new UsernameNotFoundException("Error: user '\" + username + \"' does not exist.");
		}

		List<GrantedAuthority> authorities = person.getRoles().stream()
				.map(role -> new SimpleGrantedAuthority(role.getName()))
				.peek(authority -> logger.info("Role: " + authority.getAuthority())).collect(Collectors.toList());

		return new User(person.getUsername(), person.getPassword(), true, true, true, true, authorities);
		
	}

	@Override
	public List<Person> findAll() {
		return personRepo.findAll();
	}

	@Override
	public Page<Person> findAll(Pageable pageable) {
		return personRepo.findAll(pageable);
	}

	@Override
	public Person findById(Long id) {
		return personRepo.findById(id).orElse(null);
	}

	@Override
	public Person findByUsername(String username) {
		return personRepo.findByUsername(username);
	}

	@Override
	public Person save(Person person) {
		return personRepo.save(person);
	}

	@Override
	public void delete(Long id) {
		personRepo.deleteById(id);
	}

}
