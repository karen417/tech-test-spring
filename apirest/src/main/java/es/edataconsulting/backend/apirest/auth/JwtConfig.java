package es.edataconsulting.backend.apirest.auth;

public class JwtConfig {
	
	public static final String RSA_PRIVATE = "-----BEGIN RSA PRIVATE KEY-----\r\n" + 
			"MIIEowIBAAKCAQEAoxdchGsLYoPEZdY6YGof+smDyiUZzgkFPOmhiFQa0lYLGAR+\r\n" + 
			"GG+Mt0SqTNQf2GUHZtccKrRFvzvb27s+KXVL3d+qGrmwfluHCe+BfWUcv+V65CLU\r\n" + 
			"CbkFMAOVqbLP2ob4KHQu51n+fjCB/cTBay8Vr5T3zzwwnKFIgtGZ0w01GuII+5vc\r\n" + 
			"r51/0SKQJMOwWjrl3Kil8/W85klLwWgiUYM3cPW33LGqW732KJiAPU7YpfHRD2ya\r\n" + 
			"1IA+k8RejV2oS7PxIYUbDW0dUTgjVa70/tg2gcyMWOs0uZK2uT+VsjK4A9+VU72/\r\n" + 
			"bv1WCi3HXtU+/JVO5xrdOjmnSE7gr0F+yB/BAwIDAQABAoIBADe5zsHsNYqmzV+i\r\n" + 
			"kHXLLTwqyN40SRnuWRzUE36ahkOiZM4DEvbUfrdMPghcXrk45AuOqrAowsAx+tCc\r\n" + 
			"rQPLcW3Rc7UIB7dVHNIXRR6MgwNKrO/wupTL5Grgfq3qTD016usfr7sbM+r4NiJ4\r\n" + 
			"rUIrZ3SEKRxlS5LpRPZ2GeJUW/XgBF/O4ETf7mJMguTlapmtyEE0iu9XpdQ5M9X7\r\n" + 
			"ZZnIboXbMY+8UGpj3OnKgMv0xeyqrQ8b4pt7s0kRHccmugDtFZkHN3bvSX4HrLlU\r\n" + 
			"/V/sC54jdxYLa5gDYvxJRf1oDnMY0+4qTNNiEZvtSaR+Vt47J1ExP1Pkhlu86aeP\r\n" + 
			"McRxIlECgYEAze8HCBZniebsNmI/s4zitSLYthfzPjxngdkpv7hm0grdT2Xp9R9U\r\n" + 
			"RSvt5w4TIW7E5HULJ+bnfn4PGPuQwGgFQeijKs6uZCWrr2/4wBz1sJrWcOmcVhai\r\n" + 
			"PzFCq/2y7j/oL4VP2j+MgddJOn7pnPvwbZLG5bFDVAMr0nN75XJq08kCgYEAyr3k\r\n" + 
			"HIs3Lk6ZYpSuOT4nC1fFwr/eWxaXAqd9RzRhxjbfVYKi/HYtIb8E/z7U5XcHUMv8\r\n" + 
			"QT1juCF0q6Ti5kUeFa/td8V8sqOEAda3TrJ4kFOCNJ0t8wDsVwWc4Tbzahxp6cSG\r\n" + 
			"wqErPdT0vj2+h2S1mWIKRPacDd+7EHgCctL1XGsCgYEAtXSf+oVbWEzM6KKrxLco\r\n" + 
			"cDkzQUhbQgSRo/3JZrDCkOOX3EVN4Hppk8XAi+5/9t6WyUNEwDUa1/+VqArKom9F\r\n" + 
			"6qxlUevqCD6VSDHeutEJwEK/H6EI62ZoMkxilhAm4P4Yxf6uvP1v/lzHnq6DhJsO\r\n" + 
			"YE6LyYrna5ibvYWarzOhY9kCgYBfz2tMnfG+3YAZGUBYZunWr20Sws+W5k8I/xPy\r\n" + 
			"IWwPWwEiDPFvM33Sk84NvaiopB597VhRkGxeqNjVAMAFqCY2g4RKdLagSSvqHolU\r\n" + 
			"FH1rXzuKF7mOPCJ0YX+o4uoXfEQFKE2zDq07tDbQ7Kbsnwg+6xkFTDGlUNS7TKuD\r\n" + 
			"jfRL7wKBgDxyAhqAbJRWOY526nQKwLHB3YDBf6islDKX7Y1Dj6DwI4Kss198M4Hu\r\n" + 
			"RsHSgaGvUKAOZS86hZDSvJrm1DsdbOxJlEejc2HjksofE/B5/YpboHeHqL+yncVe\r\n" + 
			"Ux/HNvf5wnURd594rzUUdnxevPKa6WJILS4adVq1oxrHOwpTT7yQ\r\n" + 
			"-----END RSA PRIVATE KEY-----";
	
	public static final String RSA_PUBLIC = "-----BEGIN PUBLIC KEY-----\r\n" + 
			"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoxdchGsLYoPEZdY6YGof\r\n" + 
			"+smDyiUZzgkFPOmhiFQa0lYLGAR+GG+Mt0SqTNQf2GUHZtccKrRFvzvb27s+KXVL\r\n" + 
			"3d+qGrmwfluHCe+BfWUcv+V65CLUCbkFMAOVqbLP2ob4KHQu51n+fjCB/cTBay8V\r\n" + 
			"r5T3zzwwnKFIgtGZ0w01GuII+5vcr51/0SKQJMOwWjrl3Kil8/W85klLwWgiUYM3\r\n" + 
			"cPW33LGqW732KJiAPU7YpfHRD2ya1IA+k8RejV2oS7PxIYUbDW0dUTgjVa70/tg2\r\n" + 
			"gcyMWOs0uZK2uT+VsjK4A9+VU72/bv1WCi3HXtU+/JVO5xrdOjmnSE7gr0F+yB/B\r\n" + 
			"AwIDAQAB\r\n" + 
			"-----END PUBLIC KEY-----";

}
