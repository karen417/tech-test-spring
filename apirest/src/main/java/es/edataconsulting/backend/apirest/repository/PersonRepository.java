package es.edataconsulting.backend.apirest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import es.edataconsulting.backend.apirest.entity.Person;

public interface PersonRepository extends JpaRepository<Person, Long> {

	public Person findByUsername(String username);

}
