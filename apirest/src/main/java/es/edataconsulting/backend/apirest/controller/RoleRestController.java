package es.edataconsulting.backend.apirest.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.edataconsulting.backend.apirest.entity.Role;
import es.edataconsulting.backend.apirest.service.IRoleService;

@CrossOrigin(origins= {"http://localhost:4200"})
@RestController
@RequestMapping("/api/roles")
public class RoleRestController {

	@Autowired
	private IRoleService roleService;
	
	@GetMapping("/")
	public List<Role> index() {
		return roleService.findAll();
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> show(@PathVariable Long id) {
		
		Role role = null;

		Map<String, Object> response = new HashMap<>();

		try {
			role = roleService.findById(id);
		} catch (DataAccessException e) {
			response.put("message", "Data access error.");
			response.put("error", e.getMessage() + ": " + e.getMostSpecificCause().getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (role == null) {
			response.put("message", "Role id " + id.toString() + " does not exist.");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Role>(role, HttpStatus.OK);
		
	}

	@Secured({"ROLE_ADMIN"})
	@PostMapping("/")
	public ResponseEntity<?> create(@Valid @RequestBody Role role, BindingResult result) {

		Role newRole = null;
		Map<String, Object> response = new HashMap<>();

		if (result.hasErrors()) {
			List<String> errors = result.getFieldErrors().stream()
					.map(err -> "Field '" + err.getField() + "' " + err.getDefaultMessage())
					.collect(Collectors.toList());

			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		try {
			newRole = roleService.save(role);
		} catch (DataAccessException e) {
			response.put("message", "Data access error.");
			response.put("error", e.getMessage() + ": " + e.getMostSpecificCause().getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("message", "The role has been successfully created!");
		response.put("role", newRole);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

	}

	@Secured({"ROLE_ADMIN"})
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody Role role, BindingResult result, @PathVariable Long id) {

		Role updatedRole = null;
		Role actualRole = null;
		Map<String, Object> response = new HashMap<>();

		if (result.hasErrors()) {
			List<String> errors = result.getFieldErrors().stream()
					.map(err -> "Field '" + err.getField() + "' " + err.getDefaultMessage())
					.collect(Collectors.toList());
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		try {
			actualRole = roleService.findById(id);
		} catch (DataAccessException e) {
			response.put("message", "Data access error.");
			response.put("error", e.getMessage() + ": " + e.getMostSpecificCause().getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (actualRole == null) {
			response.put("message", "Role id" + id.toString() + " does not exist.");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		actualRole.setName(role.getName());

		try {
			updatedRole = roleService.save(actualRole);
		} catch (DataAccessException e) {
			response.put("message", "Data access error.");
			response.put("error", e.getMessage() + ": " + e.getMostSpecificCause().getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("message", "The role has been successfully updated!");
		response.put("role", updatedRole);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
		
	}

	@Secured({"ROLE_ADMIN"})
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		
		Map<String, Object> response = new HashMap<>();

		try {
			roleService.delete(id);
		} catch (DataAccessException e) {
			response.put("message", "Data access error.");
			response.put("error", e.getMessage() + ": " + e.getMostSpecificCause().getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("message", "The role has been successfully deleted!");

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		
	}
}
