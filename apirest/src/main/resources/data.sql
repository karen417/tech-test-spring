/* Populate table TM_USER */
INSERT INTO TM_USER (ID, NAME, USERNAME, PASSWORD) VALUES (1, 'User', 'user', '$2a$10$66BUknptjoC3scqoSWzcBePkqdKM1ZoX5XXmjXLDGRr5E29DDceUa');
INSERT INTO TM_USER (ID, NAME, USERNAME, PASSWORD) VALUES (2, 'Felipe', 'felipe', '$2a$10$66BUknptjoC3scqoSWzcBePkqdKM1ZoX5XXmjXLDGRr5E29DDceUa');
INSERT INTO TM_USER (ID, NAME, USERNAME, PASSWORD) VALUES (3, 'Roberto', 'roberto', '$2a$10$66BUknptjoC3scqoSWzcBePkqdKM1ZoX5XXmjXLDGRr5E29DDceUa');
INSERT INTO TM_USER (ID, NAME, USERNAME, PASSWORD) VALUES (4, 'Maria', 'maria', '$2a$10$66BUknptjoC3scqoSWzcBePkqdKM1ZoX5XXmjXLDGRr5E29DDceUa');
INSERT INTO TM_USER (ID, NAME, USERNAME, PASSWORD) VALUES (5, 'Rebeca', 'rebeca', '$2a$10$66BUknptjoC3scqoSWzcBePkqdKM1ZoX5XXmjXLDGRr5E29DDceUa');

/* Populate table TM_ROLE */
INSERT INTO TM_ROLE (ID, NAME) VALUES (1, 'ROLE_ADMIN');
INSERT INTO TM_ROLE (ID, NAME) VALUES (2, 'ROLE_STANDARD');

/* Populate table TM_USER_ROLES */
INSERT INTO TM_USER_ROLES (PERSON_ID, ROLES_ID) VALUES (1, 1);
INSERT INTO TM_USER_ROLES (PERSON_ID, ROLES_ID) VALUES (1, 2);
INSERT INTO TM_USER_ROLES (PERSON_ID, ROLES_ID) VALUES (2, 1);
INSERT INTO TM_USER_ROLES (PERSON_ID, ROLES_ID) VALUES (2, 2);
INSERT INTO TM_USER_ROLES (PERSON_ID, ROLES_ID) VALUES (3, 2);
INSERT INTO TM_USER_ROLES (PERSON_ID, ROLES_ID) VALUES (4, 2);
INSERT INTO TM_USER_ROLES (PERSON_ID, ROLES_ID) VALUES (5, 1);